export interface ICustomer {
  id?: number;
  firstName?: string;
  lastName?: string;
}

export const defaultValue: Readonly<ICustomer> = {};
