import { Moment } from 'moment';

export const enum Currency {
  GBP = 'GBP',
  EUR = 'EUR',
  USD = 'USD'
}

export const enum OfferStatus {
  CREATED = 'CREATED',
  ACTIVE = 'ACTIVE',
  CANCELLED = 'CANCELLED'
}

export interface IOffer {
  id?: number;
  title?: string;
  expiryDate?: Moment;
  description?: string;
  price?: number;
  currency?: Currency;
  creationDate?: Moment;
  offerStatus?: OfferStatus;
  thumbnailPhotoContentType?: string;
  thumbnailPhoto?: any;
  thumbnailPhotoUrl?: string;
  fullPhotoContentType?: string;
  fullPhoto?: any;
  fullPhotoUrl?: string;
  merchantFirstName?: string;
  merchantId?: number;
  customerFirstName?: string;
  customerId?: number;
}

export const defaultValue: Readonly<IOffer> = {};
