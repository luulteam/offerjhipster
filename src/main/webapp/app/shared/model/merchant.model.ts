export interface IMerchant {
  id?: number;
  firstName?: string;
  lastName?: string;
}

export const defaultValue: Readonly<IMerchant> = {};
