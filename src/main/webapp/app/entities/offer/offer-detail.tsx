import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, openFile, byteSize, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './offer.reducer';
import { IOffer } from 'app/shared/model/offer.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOfferDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class OfferDetail extends React.Component<IOfferDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { offerEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="offersaleApp.offer.detail.title">Offer</Translate> [<b>{offerEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="title">
                <Translate contentKey="offersaleApp.offer.title">Title</Translate>
              </span>
            </dt>
            <dd>{offerEntity.title}</dd>
            <dt>
              <span id="expiryDate">
                <Translate contentKey="offersaleApp.offer.expiryDate">Expiry Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={offerEntity.expiryDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="description">
                <Translate contentKey="offersaleApp.offer.description">Description</Translate>
              </span>
            </dt>
            <dd>{offerEntity.description}</dd>
            <dt>
              <span id="price">
                <Translate contentKey="offersaleApp.offer.price">Price</Translate>
              </span>
            </dt>
            <dd>{offerEntity.price}</dd>
            <dt>
              <span id="currency">
                <Translate contentKey="offersaleApp.offer.currency">Currency</Translate>
              </span>
            </dt>
            <dd>{offerEntity.currency}</dd>
            <dt>
              <span id="creationDate">
                <Translate contentKey="offersaleApp.offer.creationDate">Creation Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={offerEntity.creationDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="offerStatus">
                <Translate contentKey="offersaleApp.offer.offerStatus">Offer Status</Translate>
              </span>
            </dt>
            <dd>{offerEntity.offerStatus}</dd>
            <dt>
              <span id="thumbnailPhoto">
                <Translate contentKey="offersaleApp.offer.thumbnailPhoto">Thumbnail Photo</Translate>
              </span>
            </dt>
            <dd>
              {offerEntity.thumbnailPhoto ? (
                <div>
                  <a onClick={openFile(offerEntity.thumbnailPhotoContentType, offerEntity.thumbnailPhoto)}>
                    <img
                      src={`data:${offerEntity.thumbnailPhotoContentType};base64,${offerEntity.thumbnailPhoto}`}
                      style={{ maxHeight: '30px' }}
                    />
                  </a>
                  <span>
                    {offerEntity.thumbnailPhotoContentType}, {byteSize(offerEntity.thumbnailPhoto)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <span id="thumbnailPhotoUrl">
                <Translate contentKey="offersaleApp.offer.thumbnailPhotoUrl">Thumbnail Photo Url</Translate>
              </span>
            </dt>
            <dd>{offerEntity.thumbnailPhotoUrl}</dd>
            <dt>
              <span id="fullPhoto">
                <Translate contentKey="offersaleApp.offer.fullPhoto">Full Photo</Translate>
              </span>
            </dt>
            <dd>
              {offerEntity.fullPhoto ? (
                <div>
                  <a onClick={openFile(offerEntity.fullPhotoContentType, offerEntity.fullPhoto)}>
                    <img src={`data:${offerEntity.fullPhotoContentType};base64,${offerEntity.fullPhoto}`} style={{ maxHeight: '30px' }} />
                  </a>
                  <span>
                    {offerEntity.fullPhotoContentType}, {byteSize(offerEntity.fullPhoto)}
                  </span>
                </div>
              ) : null}
            </dd>
            <dt>
              <span id="fullPhotoUrl">
                <Translate contentKey="offersaleApp.offer.fullPhotoUrl">Full Photo Url</Translate>
              </span>
            </dt>
            <dd>{offerEntity.fullPhotoUrl}</dd>
            <dt>
              <Translate contentKey="offersaleApp.offer.merchant">Merchant</Translate>
            </dt>
            <dd>{offerEntity.merchantFirstName ? offerEntity.merchantFirstName : ''}</dd>
            <dt>
              <Translate contentKey="offersaleApp.offer.customer">Customer</Translate>
            </dt>
            <dd>{offerEntity.customerFirstName ? offerEntity.customerFirstName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/offer" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/offer/${offerEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ offer }: IRootState) => ({
  offerEntity: offer.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OfferDetail);
