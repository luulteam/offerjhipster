import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './merchant.reducer';
import { IMerchant } from 'app/shared/model/merchant.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IMerchantDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class MerchantDetail extends React.Component<IMerchantDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { merchantEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="offersaleApp.merchant.detail.title">Merchant</Translate> [<b>{merchantEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="firstName">
                <Translate contentKey="offersaleApp.merchant.firstName">First Name</Translate>
              </span>
            </dt>
            <dd>{merchantEntity.firstName}</dd>
            <dt>
              <span id="lastName">
                <Translate contentKey="offersaleApp.merchant.lastName">Last Name</Translate>
              </span>
            </dt>
            <dd>{merchantEntity.lastName}</dd>
          </dl>
          <Button tag={Link} to="/entity/merchant" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/merchant/${merchantEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ merchant }: IRootState) => ({
  merchantEntity: merchant.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MerchantDetail);
