package com.els.offer.service.mapper;

import com.els.offer.domain.*;
import com.els.offer.service.dto.OfferDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Offer and its DTO OfferDTO.
 */
@Mapper(componentModel = "spring", uses = {MerchantMapper.class, CustomerMapper.class})
public interface OfferMapper extends EntityMapper<OfferDTO, Offer> {

    @Mapping(source = "merchant.id", target = "merchantId")
    @Mapping(source = "merchant.firstName", target = "merchantFirstName")
    @Mapping(source = "customer.id", target = "customerId")
    @Mapping(source = "customer.firstName", target = "customerFirstName")
    OfferDTO toDto(Offer offer);

    @Mapping(source = "merchantId", target = "merchant")
    @Mapping(source = "customerId", target = "customer")
    Offer toEntity(OfferDTO offerDTO);

    default Offer fromId(Long id) {
        if (id == null) {
            return null;
        }
        Offer offer = new Offer();
        offer.setId(id);
        return offer;
    }
}
