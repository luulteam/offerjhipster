/**
 * View Models used by Spring MVC REST controllers.
 */
package com.els.offer.web.rest.vm;
