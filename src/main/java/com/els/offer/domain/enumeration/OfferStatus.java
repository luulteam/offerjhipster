package com.els.offer.domain.enumeration;

/**
 * The OfferStatus enumeration.
 */
public enum OfferStatus {
    CREATED, ACTIVE, CANCELLED
}
