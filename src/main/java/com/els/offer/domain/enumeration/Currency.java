package com.els.offer.domain.enumeration;

/**
 * The Currency enumeration.
 */
public enum Currency {
    GBP, EUR, USD
}
